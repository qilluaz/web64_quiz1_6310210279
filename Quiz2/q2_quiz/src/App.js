import './App.css';
import { Routes, Route } from "react-router-dom";
import Header from './components/Header'
import AboutUsPage from './pages/AboutUsPage';
import MuPage from './pages/MuPage';


function App() {
  return (
    <div className="App">
      <Header />
      <Routes>

          <Route path="about" element ={
              <AboutUsPage/>
          }/>

          <Route path="/" element ={
              <MuPage/>
          }/>

      </Routes>
    </div>
  );
}

export default App;
