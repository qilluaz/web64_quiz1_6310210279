import MuResult from '../components/MuResult.js'
import {useState} from "react";

import Button from '@mui/material/Button';
import Grid  from '@mui/material/Grid';
import { Typography, Box } from '@mui/material';

function MuPage() {

    const [ Name, setName ] = useState("");
    const [ Results, setResults ] = useState("");

    const [Day, setDay] = useState("");
    const [BE, setBE] = useState("");

    function Mutelu(){
        let Day = parseFloat(Day);
        let BE = parseFloat(BE);
        let Results = Day*(BE/2);
        
        if(Results>1000){
            setResults("คุณอาจจะพบใครคนหนึ่งที่คุณคิดว่าพิเศษสำหรับคุณ อาจนะ แค่อาจ");
        }else
            setResults("ความรักของคุณกำลังจะได้รับการทดสอบ คุณจะรู้สึกได้ถึงแรงกระตุ้นของความรักทั้งทางร่างกายและจิตใจในระยะนี้ เขาหรือเธอที่คุณพบจะเป็นคนจิตใจดีและมั่นคง");
        
    }

    return(

        <Grid container spacing={2} sx={{ marginTop : "10px "}}> 
        <Grid item xs={12}>
            <Typography variant='h4'>
            
            <h2> มูเตลู Ooh wa ah ah ah</h2> 
            <h2> มูเตลู Ooh wa ah ah ah</h2> 
            <h2> มูเตลู Ooh wa ah ah ah</h2>
            <h2> มูเตลู Ooh wa ah ah ah</h2> 
            <h2> มูเตลู </h2>
            <br/>
            <h2>เสกเธอให้หันมาสบตา</h2>
            <h2>บุพเพ สันนิ เสนะ Hey</h2>

            </Typography>
        </Grid>
        <Grid item xs={8}>
<Box sx={{ textAlign : "canter"}} > <br /> <br /> <br /> <br /> <br /> <br />
        ชื่อ : <input type = "text" 
        value={Name} onChange={(e) => { setName(e.target.value);}}/> 
        <br /><br /> 
        
        วันที่เกิด (1-31)  : <input type = "text" 
        value={Day} onChange={(e) =>{setDay(e.target.value);}}/>  
        <br /> <br /> 

        ปีที่เกิด (พ.ศ.) : <input type = "text" 
        value={BE} onChange={(e) => {setBE(e.target.value);}}/>   <br /> <br /> <br />

        <Button variant="contained" onClick={ () => {Mutelu()}}>
            บาลาชู ลาชุ เพี้ยง!!
        </Button>
</Box>
        </Grid>
        <Grid item xs={4}>  <br /> <br /> <br /> 
        { 
                <div>
                    <h3>ผลการทำนาย</h3>
                    <MuResult
                        Name ={ Name }
                        Results = { Results }
                    />
                </div>
            }
        </Grid>
    </Grid>

    
    );
}

export default MuPage;
