import {Link} from "react-router-dom"

import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';

export default function ButtonAppBar() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
       
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }} >

            <MenuIcon />
            <Link to="/">  คำทำนาย  </Link>
                &nbsp; &nbsp; &nbsp;
                <Link to="about">  รู้จักผู้ทำนาย  </Link>
                &nbsp; &nbsp; &nbsp;
                
          </IconButton>

          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
          </Typography>

          <Button color="inherit">โอมเพี้ยง!!</Button>

        </Toolbar>
      </AppBar>
    </Box>
  );
}
