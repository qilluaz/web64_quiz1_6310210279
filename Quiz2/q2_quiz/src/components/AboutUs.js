import './AboutUs.css'

function AboutUs (props){


    return (
        <div class="BG">
            <h2> ให้คำทำนายโดย { props.name } </h2>
            <h3> ผู้มาจาก { props.info }.</h3>
            <br />
            <h3> 6310210279</h3>
            <h3>พัตราภรณ์  กูมุดา</h3>
        </div>

    );
}

export default AboutUs;